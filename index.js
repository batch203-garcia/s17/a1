/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/

//first function here:

function userInformation() {
    let userFullName = prompt("Enter your Full Name");
    let userAge = prompt("Enter your Age");
    let userLocation = prompt("Enter your Location");

    console.log("Hello, " + userFullName);
    console.log("You are " + userAge + " years old.");
    console.log("You live in " + userLocation);
}

userInformation();

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

function favoriteBands() {
    console.log("1. The Beatles");
    console.log("2. The Metallica");
    console.log("3. The Eagles");
    console.log("4. The Parokya ni Edgar");
    console.log("5. The Eheads");
}

favoriteBands();

//second function here:

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
function favoriteMovies() {
    console.log("1. The God Father");
    console.log("Rotten Tomatoes Rating: 97%");
    console.log("2. The God Father, Part II");
    console.log("Rotten Tomatoes Rating: 96%");
    console.log("3. Shawshank Redemption");
    console.log("Rotten Tomatoes Rating: 91%");
    console.log("4. To Kill A Mocking Bird");
    console.log("Rotten Tomatoes Rating: 93%");
    console.log("5. Psycho");
    console.log("Rotten Tomatoes Rating: 96%");

}

favoriteMovies();

//third function here:

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/


let printFriends = function printUsers() {
    alert("Hi! Please add the names of your friends.");
    let friend1 = prompt("Enter your first friend's name:");
    let friend2 = prompt("Enter your second friend's name:");
    let friend3 = prompt("Enter your third friend's name:");

    console.log("You are friends with:")
    console.log(friend1);
    console.log(friend2);
    console.log(friend3);
};

printFriends();